# Five Random walkers
import random
import numpy as np

N = 100
M = 5

# Generate vector of distance walked by the random walkers
YN = []
for m in range(0,M):	# Initializing all the positions in zero
	YN.append(0)

arq = open('Mwalkers.dat','w')

# Evolution in time
for i in range(0,N):
	wline = str(i)+'\t'
	for j in range(0,M):	# For each walker create a random step
		randx = random.random()	# Random value
		if(randx<0.5):
			deltax=1
		else:
			deltax=-1
		
		YN[j]=YN[j]+deltax	# Walking of the walker
		wline = wline+str(YN[j])+'\t'
	wline = wline+' \n'
	arq.write(wline)

arq.close()

L = 1000
RY = []	# Random generate values
delta = 0.01
for i in range(0,L):
	randt = random.random()
	RY.append(randt)

Ymax = np.amax(RY)
Ymin = np.amin(RY)
Nd = int(1/delta)

hist = []
for l in range(0,Nd+1):
	hist.append(0)

for j in range(0,Nd+1):
	index = int(RY[j]/delta)-int(Ymin/delta)
	hist[index] = hist[index]+1

arq = open('uniforme.dat','w')
y = Ymin+(delta/2.)
arq.write(str(y)+'\t'+str(hist[0]/(float(L)))+'\n')
for k in range(1,Nd):
	y = y+delta	
	arq.write(str(y)+'\t'+str(hist[k]/(float(L)))+'\n')
arq.close()
