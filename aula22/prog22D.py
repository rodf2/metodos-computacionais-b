# Questao 2, transform distribuition os steps

import random
import numpy as np

def f_int(ymin,ymax): # Integrates the g(y) functional form
	ND = 1000	# Number of divisions
	dy = (ymax-ymin)/ND	# "differential"
	I = 0.		# Initialize integration sum 
	y = ymin	# Initialize integration variable
	for i in range(0,ND):	# Integration
		I = I + (0.5*np.exp(-y/2.)*dy)
		y = y+dy
	return I

def DisTrsf(x,Nm):	# Transformation x --> y
	# Transform the uniform (f(x)=1) distribution in g(y)=(1/2)exp(-y/2)/Nm
	y = -2*np.ln(1-Nm*x) 
	return y
	
N = 100000	# Number of random values
M = 100		# Number of divisions for the histogram
xmin = 0.	# Limits in the x space
xmax = 1.	
ymin = 0.	# Limits in the y space
ymax = np.pi
Nm = f_int(ymin,ymax)	# Normalization constant	
delta1 = (xmax-xmin)/M	# Increment in x space
delta2 = (ymax-ymin)/M	# Increment in y space

hist1 = []	# Histogram for uniform distribution
hist2 = []	# Histogram for sine distribuition

# Generate histograms elements
for j in range(0,M):
	hist1.append(0)	
	hist2.append(0)

# Generate random values and count them
for i in range(1,N):	
	X1 = random.random()	# Pseudo-random value
	Y1 = DisTrsf(X1,Nm)	# Inverse transformation sine FDP

	# Check index of the interval in which a random value lies
	index1 = int(X1/delta1)	
	index2 = int(Y1/delta2)

	# Assign to the correspond interval in the histogram
	hist1[index1] = hist1[index1]+1
	hist2[index2] = hist2[index2]+1

arq1 = open('uniforme.dat','w')	
for k in range(0,M):
	x1 = (delta1/2.)+k*delta1
	arq1.write(str(x1)+'\t'+str(hist1[k]/float(N))+'\n')
arq1.close()

arq2 = open('seno.dat','w')
for l in range(0,M):
	x2 = (delta2/2.)+l*delta2
	arq2.write(str(x2)+'\t'+str(hist2[l]/float(N))+'\n')
arq2.close()

