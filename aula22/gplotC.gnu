reset 

set encoding utf8 
set xlabel "Yn" 
set ylabel "f(x)" 
set grid 
set term post eps enhanced color 
set xrange[-300:300]
set style fill transparent
set title "Histogramas para FDP de caminhantes aleatórios"
set out "hist_YN.eps" 
set boxwidth 1.0	# delta/2

a1 = 1
sigma1 = 10
mu1 = 1
a2 = 1
sigma2 = 50
mu2 = 1
a3 = 1
sigma3 = 150
mu3 = 1 
f(x) = a1/(sigma1*sqrt(2.*pi))*exp(-(x-mu1)**2/(0.5*sigma1**2))
g(x) = a2/(sigma2*sqrt(2.*pi))*exp(-(x-mu2)**2/(0.5*sigma2**2))
h(x) = a3/(sigma3*sqrt(2.*pi))*exp(-(x-mu3)**2/(0.5*sigma3**2))
fit f(x) 'hist_YN_t100.dat' via a1, sigma1, mu1
fit g(x) 'hist_YN_t1000.dat' via a2, sigma2, mu2
fit h(x) 'hist_YN_t10000.dat' via a3, sigma3, mu3

plot 'hist_YN_t100.dat' with boxes t "N = 100", \
	'hist_YN_t1000.dat' with boxes t "N = 1000", \
	'hist_YN_t10000.dat' with boxes t "N = 10000", \
	f(x) w l lw 3 lc 'black', \
	g(x) w l lw 3 lc 'black', \
	h(x) w l lw 3 lc 'black'
#pause -1
