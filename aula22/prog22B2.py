import random
import numpy as np

def DisTrsf(x,Nm):	 # Transformation x --> y
	# Transform the uniform distribuition in exponential decay
	# f(x) = 1 --> g(y) = (1/2)*exp(-y/2)
	y = -2*np.log(1-Nm*x)
	return y

N = 1000
M = 10000
Nm = 1.

# Generate vector of distance walked by the random walkers
YN = []
for m in range(0,M):	# Initializing all the positions in zero
	YN.append(0)

arq = open('sigmavsN2.dat','w')

# Evolution in time
for i in range(0,N):
	m1sum =	0	# First moment sum in a given time
	m2sum = 0	# Second moment sum in a given time
	for j in range(0,M):	# For each walker create a random step
		randx = random.random()	# Random value
		# Transform to exponential distribuition
		randy = DisTrsf(randx,Nm)
		
		randd = random.random()
		if(randd<0.5):
			deltay=randy
		else:
			deltay=-randy
		
		YN[j]=YN[j]+deltay	# Walking of the walker
	
		# Iterates sum
		m1sum = m1sum+YN[j]		
		m2sum = m2sum+(YN[j]*YN[j])

	# First and second moments in a given time
	m1 = m1sum/M
	m2 = m2sum/M
	
	# Mean square displacement in a given time
	msd = np.sqrt(m2-(m1*m1)) 

	arq.write(str(i)+'\t'+str(msd)+'\n')

arq.close()
