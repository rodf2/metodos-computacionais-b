# Histograms
import random
import numpy as np

def DisTrsf(x,Nm):      # Transformation x --> y
        # Transform the uniform distribuition in exponential decay
        # f(x) = 1 --> g(y) = (1/2)*exp(-y/2)
        y = -2*np.log(1-Nm*x)
        return y
	
N = [100,1000,10000]	# 3 different time limits
M = 10000	# Number of walkers
Nm = 1.		# Normalization of transformation
delta = 4.	# Bin size

for n in range(0,len(N)):	# For each time limit

	print('Calculating for '+str(N[n])+' walkers...')
	
	# Vector of distance walked by each random walker
	YN = []
	for m in range(0,M):	# Initializing all the positions in zero
		YN.append(0)

	# Evolution in time
	for i in range(0,N[n]):
		for j in range(0,M):	# For each walker create a random step
			randx = random.random()	# Random value
			stp = DisTrsf(randx,Nm)	# Inverse Transformation

			# New random value to decide direction of the step
			randd=random.random()
			if(randd<0.5):
				dy=stp
			else:
				dy=-stp
			
			YN[j]=YN[j]+dy	# Walking of the walker
	
	Ymin = 	np.amin(YN) # Minimum possible last position
	Ymax = 	np.amax(YN) # Maximum possible last position

	Nd = int((Ymax-Ymin)/delta)	# Number of divisions of histogram
	
	hist=[]	# Histogram 
	for h in range(0,Nd+1):	# Initialize histogram
		hist.append(0)
	
	# Construct the histogram values
	for j in range(0,M):
		# Check index of the interval in which a value lies
		index = int(YN[j]/delta)-int(Ymin/delta)
		# Assign to the correspond interval in the histogram
		hist[index] = hist[index]+1

	totprob = 0.	# MUST BE 1
	totarea = 0.

	# Estimate total area of histogram
	for j in range(0,len(hist)):
		totarea = totarea+hist[j]*delta

	# Verify if total probability is 1
	for k in range(0,len(hist)):
		totprob = totprob+hist[k]*delta/totarea
	
	print(totprob)

	# Save data
	arq = open('hist_YN2_t'+str(N[n])+'.dat','w')
	
	# Starts at ymin positioning the bars i nthe center of the interval
	y = Ymin+(delta/2.)
	arq.write(str(y)+'\t'+str(hist[0]*delta/totarea)+'\n')

	# Write the rest of the lines
	for k in range(1,Nd):
		y = y+delta
		arq.write(str(y)+'\t'+str(hist[k]*delta/totarea)+'\n')
	arq.close()
