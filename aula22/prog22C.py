# Histograms
import random
import numpy as np

N = [100,1000,10000]	# 3 different time limits
M = 10000	# Number of walkers
stp = 1.0 # Size of random step of the walker

for n in range(0,len(N)):	# For each time limit

	print('Calculating for '+str(N[n])+' walkers...')
	
	Ymin = -stp*N[n]	# Minimum possible last position
	Ymax = stp*N[n]		# Maximum possible last position
	Nd = N[n]		# Number of divisions of the histogram
	delta = (Ymax-Ymin)/float(Nd)	# Bin width

	hist=[]	# Histogram 
	for h in range(0,Nd):	# Initialize histogram
		hist.append(0)

	# Vector of distance walked by each random walker
	YN = []
	for m in range(0,M):	# Initializing all the positions in zero
		YN.append(0)

	# Evolution in time
	for i in range(0,N[n]):
		for j in range(0,M):	# For each walker create a random step
			randx = random.random()	# Random value
			if(randx<0.5):
				dx=stp
			else:
				dx=-stp
			
			YN[j]=YN[j]+dx	# Walking of the walker
	
	# Construct the histogram values
	for j in range(0,M):
		# Check index of the interval in which a value lies
		index = int(YN[j]/delta)-int(Ymin/delta)
		# Assign to the correspond interval in the histogram
		hist[index] = hist[index]+1

	totprob = 0.	# MUST BE 1
	totarea = 0.

	# Estimate total area of histogram
	for j in range(0,len(hist)):
		totarea = totarea+hist[j]*delta		

	# Verify if total probability is 1
	for k in range(0,len(hist)):
		totprob = totprob+hist[k]*delta/totarea
	print(totprob)

	# Save data
	arq = open('hist_YN_t'+str(N[n])+'.dat','w')
	
	# Starts at ymin positioning the bars in the center of the interval
	y = Ymin+(delta/2.)
	arq.write(str(y)+'\t'+str(hist[0]*delta/totarea)+'\n')

	# Write the rest of the lines
	for k in range(1,Nd):
		y = y+delta
		arq.write(str(y)+'\t'+str(hist[k]*delta/totarea)+'\n')
	arq.close()
