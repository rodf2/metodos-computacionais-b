# Five Random walkers using exponential decay function for step
import random
import numpy as np

def DisTrsf(x,Nm):	# Transformation x --> y
	# Transform the uniform distribuition in exponential decay
	# f(x) = 1 --> g(y) = (1/2)*exp(-y/2)
	y = -2*np.log(1-Nm*x)
	return y

N = 100	# Number of steps
M = 5	# Number of walkers
Nm = 1.	# Normalization factor

# Generate vector of distance walked by the random walkers
YN = []
for m in range(0,M):	# Initializing all the positions in zero
	YN.append(0)

arq = open('Mwalkers2.dat','w')

# Evolution in time
for i in range(0,N):
	wline = str(i)+'\t'
	for j in range(0,M):	# For each walker create a random step
		randx = random.random()	# Random value
		randy = DisTrsf(randx,Nm)	# Inverse tranformation
		
		# New random value to decide direction of the step
		randd = random.random()
		if(randd>0.5):
			deltay = randy
		else:
			deltay = -randy

		YN[j]=YN[j]+deltay	# Walking of the walker
		wline = wline+str(YN[j])+'\t'
	wline = wline+' \n'
	arq.write(wline)

arq.close()

arq = open('exponential.dat','w')
for i in range(0,10000):
	rand1 = random.random()
	randt = DisTrsf(rand1,Nm)
	arq.write(str(i)+'\t'+str(randt)+'\n')
arq.close()
