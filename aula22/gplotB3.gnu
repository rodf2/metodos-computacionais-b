reset 

set encoding utf8 
set xlabel "N" 
set ylabel "sigma_N" 
set key left
set term post eps enhanced color 

set title "Desvio quadrático médio instantâneo"
set out "sigmavsN3.eps" 
plot 'sigmavsN3.dat' u 1:2 w p 

set logscale xy
set out "sigmavsNlog3.eps"

f(x) = b*x**a
fit f(x) 'sigmavsN3.dat' via a,b

plot 'sigmavsN3.dat' u 1:2 w p t 'Dados', f(x) w l t 'Ajuste'


#pause -1
