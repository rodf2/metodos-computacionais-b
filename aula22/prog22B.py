# Estimate MSD for each time
import random
import numpy as np

N = 1000
M = 10000

# Generate vector of distance walked by the random walkers
YN = []
for m in range(0,M):	# Initializing all the positions in zero
	YN.append(0)

arq = open('sigmavsN.dat','w')

# Evolution in time
for i in range(0,N):
	m1sum =	0	# First moment sum in a given time
	m2sum = 0	# Second moment sum in a given time
	for j in range(0,M):	# For each walker create a random step
		randx = random.random()	# Random value
		if(randx<0.5):
			deltax=1
		else:
			deltax=-1
		
		YN[j]=YN[j]+deltax	# Walking of the walker
	
		# Iterates sum
		m1sum = m1sum+YN[j]		
		m2sum = m2sum+(YN[j]*YN[j])

	# First and second moments in a given time
	m1 = m1sum/M
	m2 = m2sum/M
	
	# Mean square displacement in a given time
	msd = np.sqrt(m2-(m1*m1)) 

	arq.write(str(i)+'\t'+str(msd)+'\n')

arq.close()
