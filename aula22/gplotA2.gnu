reset 

set encoding utf8 
set xlabel "N" 
set ylabel "y_N" 
set key left
set term post eps enhanced color 

set title "Trajetória de caminhantes aleatórios (CA's)"
set out "Mwalkers2.eps" 
plot 'Mwalkers2.dat' u 1:2 w lp t "CA 1", \
	'Mwalkers2.dat' u 1:3 w lp t "CA 2", \
	'Mwalkers2.dat' u 1:4 w lp t "CA 3", \
	'Mwalkers2.dat' u 1:5 w lp t "CA 4", \
	'Mwalkers2.dat' u 1:6 w lp t "CA 5"

#pause -1
