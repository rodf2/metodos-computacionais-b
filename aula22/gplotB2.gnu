reset 

set encoding utf8 
set xlabel "N" 
set ylabel "sigma_N" 
set key left
set term post eps enhanced color 

set title "Desvio quadrático médio instantâneo"
set out "sigmavsN2.eps" 
plot 'sigmavsN2.dat' u 1:2 w p 

set logscale xy
set out "sigmavsNlog2.eps"

f(x) = b*x**a
fit f(x) 'sigmavsN2.dat' via a,b

plot 'sigmavsN2.dat' u 1:2 w p t 'Dados', f(x) w l t 'Ajuste'


#pause -1
