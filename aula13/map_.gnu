reset 

set encoding utf8 
set xlabel "n" 
set ylabel "x_n" 
set grid 
set key right 
set yrange [-1.5:3.5]

set term post eps enhanced color 
set out "map.eps" 

plot "map_u_0.2_x0_0.1_.dat" u 1:3 w lp title "mu=0.2, x0=0.1",	"map_u_0.2_x0_0.5_.dat" u 1:3 w lp title "mu=0.2, x0=0.5", "map_u_0.2_x0_0.9_.dat" u 1:3 w lp title "mu=0.2, x0=0.9", "map_u_0.5_x0_0.1_.dat" u 1:3 w lp title "mu=0.5, x0=0.1", "map_u_0.5_x0_0.5_.dat" u 1:3 w lp title "mu=0.5, x0=0.5", "map_u_0.5_x0_0.9_.dat" u 1:3 w lp title "mu=0.5, x0=0.9", "map_u_-1.0_x0_0.1_.dat" u 1:3 w lp title "mu=-1.0, x0=0.1", "map_u_-1.0_x0_0.5_.dat" u 1:3 w lp title "mu=-1.0, x0=0.5", "map_u_-1.0_x0_0.9_.dat" u 1:3 w lp title "mu=-1.0, x0=0.9"
 
