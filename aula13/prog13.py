import os

def f(u,x):
	f = u+x**2
	return f

# Variaveis necessariamente definidas
u = 0.5 
x0 = 0.9
lbl='map_'	# Label de arquivos

dtf = lbl+'u_'+str(u)+'_x0_'+str(x0)+'_.dat'

arq = open(dtf, 'w')

for i in range(0,3):
	x1 = f(u,x0)
	
	arq.write(str(i)+'\t'+str(x0)+'\t'+str(x1)+'\n')

	x0=x1

arq.close()
'''
gp = open(lbl+'.gnu', 'w')

gp.write('reset \n')
gp.write('\n')
gp.write('set encoding utf8 \n')
gp.write('set xlabel "n" \n')
gp.write('set ylabel "x_n" \n')
gp.write('set grid \n')
gp.write('set key left \n')
gp.write('\n')
gp.write('set term post eps enhanced color \n')
gp.write('set out "'+lbl+'.eps" \n')
gp.write('\n')
gp.write('plot "'+dtf+'" u 1:3 title "mu='+str(u)+', x0='+str(x0)+'", \  \n')
gp.write('\t'+dtf+'" u 1:3 title "mu='+str(u)+', x0='+str(x0)+'", \  \n')
gp.write('\t'+dtf+'" u 1:3 title "mu='+str(u)+', x0='+str(x0)+'", \  \n')
gp.write(' ')
gp.close()
'''


'''
# Escreve instrucoes p/ gnuplot
gp = open(lbl+'_'+str(u)+'.gnu', 'w')

gp.write('reset \n')
gp.write('\n')
gp.write('set encoding utf8 \n')
gp.write('set title "Logistic map (mu='+str(u)+')" \n')
gp.write('set xlabel "x_n" \n')
gp.write('set ylabel "x_{n+2}" \n')
gp.write('set grid \n')
gp.write('set key left \n')
gp.write('\n')
gp.write('set term post eps enhanced color \n')
gp.write('set out "'+lbl+'_'+str(u)+'.eps" \n')
gp.write('\n')
gp.write('set xrange [-2:2] \n')
gp.write('set yrange [-2:2] \n')
gp.write('u='+str(u)+' \n')
gp.write('f(x)=u+x*x \n')
gp.write('g(x)=(x*x*x*x)+(2*u*x*x)+(u*u)+u \n')
gp.write('h(x)=x \n')
gp.write('plot f(x) title "f(x_n)", g(x) title "f(f(x_n))", h(x) title "x_n" \n')
gp.write(' ')
gp.close()

# Executa gnuplot
os.system('gnuplot '+lbl+'_'+str(u)+'.gnu')
'''
