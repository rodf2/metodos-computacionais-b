reset 

set encoding utf8 
set title "Mapa (mu=-1)" 
set xlabel "x_n" 
set ylabel "f" 
set grid 
set key left 

set term post eps enhanced color 
set out "logmap_plot_-1.0.eps" 

set xrange [-2:2] 
set yrange [-2:2.5] 
u=-1.0
f(x)=u+x*x 
g(x)=(x*x*x*x)+(2*u*x*x)+(u*u)+u 
h(x)=x 
plot f(x) title "f(x_n)", g(x) title "f(f(x_n))", h(x) title "x_n", \
	"<echo '0 0'" with points ls 1 ps 1.3 pt 5 title "Estável: x_n = 0", \
	"<echo '-1 -1'" with points ls 1 ps 1.3 pt 5 title "Estável: x_n = -1", \
	"<echo '-0.618 -0.618'" with points ls 1 ps 1.3 pt 7 title "Estável: x_n = -0.62", \
	"<echo '1.62 1.62'" with points ls 1 ps 1.3 pt 7 title "Estável: x_n = 1.62"  
