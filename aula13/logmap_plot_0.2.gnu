reset 

set encoding utf8 
set title "Mapa (mu=0.2)" 
set xlabel "x_n" 
set ylabel "f" 
set grid 
set key left 

set term post eps enhanced color 
set out "logmap_plot_0.2.eps" 

set xrange [-1:1] 
set yrange [-0.2:1] 
u=0.2 
f(x)=u+x*x 
g(x)=(x*x*x*x)+(2*u*x*x)+(u*u)+u 
h(x)=x 
plot f(x) title "f(x_n)", g(x) title "f(f(x_n))", h(x) title "x_n", \
	"<echo '0.276 0.276'" with points ls 1 ps 1.3 pt 7 title "Instável: x_n=0.276", \
	"<echo '0.723 0.723'" with points ls 1 ps 1.3 pt 7 title "Instável: x_n=0.723"  
