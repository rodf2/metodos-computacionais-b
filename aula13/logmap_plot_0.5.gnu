reset 

set encoding utf8 
set title "Mapa (mu=0.5)" 
set xlabel "x_n" 
set ylabel "f" 
set grid 
set key left 

set term post eps enhanced color 
set out "logmap_plot_0.5.eps" 

set xrange [-3:3] 
set yrange [-1:3] 
u=0.5 
f(x)=u+x*x 
g(x)=(x*x*x*x)+(2*u*x*x)+(u*u)+u 
h(x)=x 
plot f(x) title "f(x_n)", g(x) title "f(f(x_n))", h(x) title "x_n"
