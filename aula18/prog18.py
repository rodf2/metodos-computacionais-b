# Celular automata

# Function to check each cell state in a given t
def V_30(V,N):
	
	Vr = []
	for i in range(0,N):	
		Vr.append(0)	


	for n in range(1,N-1):

		if(V[n-1]==1):	# if 1xx
			if(V[n]==0 and V[n+1]==0):	# if 100
				Vr[n]=1
			else:	# if 111 or 110 or 101
				Vr[n]=0

		else:	# if 0xx
			if(V[n]==0 and V[n+1]==0):	# if 000
				Vr[n]=0
			else:	# if 001 or 010 or 011
				Vr[n]=1
		
	return Vr

N = 300	# number of cells
M = 300	# number of time steps

Vi = []	# Initial condition vector
for i in range(0,N):	
	Vi.append(0)	

Vi[(N/2)] = 1	# Define initial condition

arq = open('automato_'+str(M)+'.dat','w')

for i in range(0,N):
	arq.write(str(0)+'\t'+str(i)+'\t'+str(Vi[i])+'\n')

V0 = Vi

for t in range(1,M):
	
	V1 = V_30(V0,N)	# Check each cell and its neighbours
	
	for i in range(0,N):
		arq.write(str(t)+'\t'+str(i)+'\t'+str(V1[i])+'\n')

	V0 = V1

arq.close()	
