# Celular automata

# Function to check each cell state in a given t
def V_30(N,V):
	for n in range(1,N-1):
		if(n==0):	# If x0x
			if(V[(n-1)]==1 and V[(n+1)]==0):	# If 100
				V[n] = 1
			if(V[(n-1)]==0 and V[(n+1)]==1):	# If 001
				V[n] = 1
			else:	# If 000 or 101
				V[n] = 0

		else:		# If x1x
			if(V[(n-1)]==1 and V[(n+1)]==1):	# If 111
				V[n] = 0
			elif(V[(n-1)]==1 and V[(n+1)]==0):	# If 110
				V[n] = 0
			else:	# If 011 or 010
				V[n] = 1

	return V

N = 10	# number of cells
M = 100 	# number of time steps

Vi = []	# Initial condition vector
for i in range(0,N):	
	Vi.append(0)	

Vi[(N/2)] = 1	# Define initial condition

arq = open('automato.dat','w')

l0 = str()	# First line

for i in range(0,N):
	l0 = l0+'\t'+str(Vi[i])	

arq.write(l0+'\n')	# Print initial condition (t=0)

V0 = Vi

for t in range(0,M):
	
	V1 = V_30(N,V0)
	
	l1 = str()
	for i in range(0,N):
		l1=l1+'\t'+str(V1[i])

	arq.write(l1+'\n')
	V0 = V1

arq.close()	
