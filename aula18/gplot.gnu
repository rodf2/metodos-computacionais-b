reset 

set encoding utf8 
set xlabel "x_i" 
set ylabel "t_i" 
set grid 
unset key 
set term post eps enhanced color 
set yrange[150:0]
set xrange[-1:300]
set title "Autômato Celular Unidimensional: Regra 30"

set out "automato_300.eps" 

plot "automato_300.dat" u 2:1:3 w image 
