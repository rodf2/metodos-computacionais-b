#include<stdio.h>
#include<math.h>

void main()
{
	double x0=10;		// Initial conditions
	double t=0, x=x0; 
	double tf=3;		// Final time 
	double h=2;		// Time increment
	double lambda=2.0;	// Decay constant
	double xa=x0;

	printf("%lf %lf %lf \n",t,x,xa);

	// Loop for integration
	while(t<tf)
	{
		// x + f(x)
		x = x/(1+lambda*h);
		t = t+h;
		xa = x0*exp(-lambda*t);

		printf("%lf %lf %lf \n",t,x,xa);
		
	}	

}
