reset 

set encoding utf8 
set xlabel "Integral numérica" 
set ylabel "Nº de pontos amostrados" 
set key left
set term post eps enhanced color 

f(x) = 0.116

set title "Integral de Monte Carlo por Amostragem Uniforme"
set out "convergencia.eps" 
	plot 'NvsIA.dat' u 1:2 w lp t "Numérico", f(x) lc rgb 'blue' t 'Valor exato'

#pause -1
