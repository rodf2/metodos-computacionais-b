import random
import numpy as np

def DisTrsf(x,Nm):	 # Transformation x --> y
	# Transform the uniform distribuition in exponential decay
	# f(r) = 1 --> g(x) = (1/2)arcsin(2r-1)
	y = 0.5*np.arcsin(2*x-1)
	return y

N = [100,1000,10000]	# Number of steps
M = 10000		# Number of walkers
Nm = 1.			# Normalization factor


for n in range(0,len(N)):
	# Generate vector of distance walked by the random walkers
	YN = []
	for m in range(0,M):	# Initializing all the positions in zero
		YN.append(0)

	# Evolution in time
	for i in range(0,N[n]):
		m1sum =	0	# First moment sum in a given time
		m2sum = 0	# Second moment sum in a given time
		for j in range(0,M):	# For each walker create a random step
			randx = random.random()	# Random value
		
			# Inverse Transform
			randy = DisTrsf(randx,Nm)
		
			# Decides the direction of the walking
			randd = random.random()
			if(randd<0.5):
				deltay=randy
			else:
				deltay=-randy
		
			YN[j]=YN[j]+deltay	# Walking of the walker
	
			# Iterates sum
			m1sum = m1sum+YN[j]		
			m2sum = m2sum+(YN[j]*YN[j])

	# First and second moments in a given time
	m1 = m1sum/M
	m2 = m2sum/M
		
	# Mean square displacement in a given time
	msd = np.sqrt(m2-(m1*m1)) 

	print('----------------------------------------')
	print('Mean Square Displacement for N = {0}: '.format(N[n]))
	print(msd)

