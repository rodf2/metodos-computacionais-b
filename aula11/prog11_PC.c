#include<stdio.h>
#include<math.h>

	// Integration of the logistic function using predictor-corrector algorithm
// dx/dt = f(x) = 0.001*x*(2-x)

double fx(double x);

void main()
{
	double x0=.01, x1, x2, x2_t;		// Initial conditions
	double t=0.; 
	double tf=8000.;		// Final time 
	double h=1.;	// Time increment
	FILE *arq;

	arq = fopen("PreCor.dat", "w");

	fprintf(arq,"%lf %lf %lf \n",t,x0);

	x1=x0+;

	// Loop for integration
	while(t<tf)
	{
		// First estimative with Adams-Bashforth (m=2) (Predictor)
		x2_t = x1+h*((3*fx(x1)/2.)-fx(x0)/2.);

		// Recalculate with Adams-Moulton (m=1) (Corrector)
		x2 = x1+h*((5*fx(x2_t)/12.)+2*(fx(x1)/3.)-(fx(x0))/12.);
	//	x2 = x1+h*fx(x2_t);
		t = t+h;
			
		fprintf(arq,"%lf %lf %lf %lf \n",t,x2_t,x2,log(fabs(x2_t-x2)));

		x0 = x1;
		x1 = x2;
	}	


	fclose(arq);
}

double fx(double x)
{
	double f;
	
	// dx/dt
	f = 0.001*x*(2-x);

	return f;
}

