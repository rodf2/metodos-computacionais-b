reset

# Line styles
set style line 1 lt 1 pt 2 linecolor rgb "red"
set style line 2 lt 1 pt 10 linecolor rgb "blue"
set style line 3 lt 1 pt 5 linecolor rgb "yellow"
set style line 4 lt 1 pt 6 linecolor rgb "green"
set style line 5 lt 1 pt 10 linecolor rgb "orange"
set style line 6 lt 1 pt 2 linecolor rgb "cyan"
set style line 7 lt 1 pt 1 linewidth 2 linecolor rgb "black"

set encoding utf8
set title "Equação logística: Adam-Muolton (S=2)"
set xlabel "t"
set ylabel "log(|x2_t-x2|)"
set grid
set key left

set term post eps enhanced color
set out 'plot_Adam_Moulton.eps'

plot "AM_0.1.dat" u 1:4 w lp ls 1 title "dt = 0.1", \
 	"AM_1.dat" u 1:4 w lp ls 2 title "dt = 1", \
	"AM_10.dat" u 1:4 w lp ls 3 title "dt = 10", \
	"AM_100.dat" u 1:4 w lp ls 4 title "dt = 100", \
	"AM_1000.dat" u 1:4 w lp ls 5 title "dt = 1000", \

#pause -1
