#include<stdio.h>
#include<math.h>

// Integration of the logistic function using Adams-Bashforth (s=1)
// dx/dt = f(x) = 0.001*x*(2-x)

double fx(double x);

void main()
{
	double x0=.01, x1, x2;		// Initial conditions
	double t=0.; 
	double tf=8000.;		// Final time 
	double h=.1;	// Time increment
	FILE *arq;

	arq=fopen("AdamBash.dat", "w");

	fprintf(arq,"%lf %lf %lf \n",t,x0);

	x1=x0;

	// Loop for integration
	while(t<tf)
	{
		// Multi-step estimation
		x2 = x1+h*((3*fx(x1)/2.)-fx(x0)/2.);
		t = t+h;
			
		fprintf(arq,"%lf %lf \n",t,x2);

		x0 = x1;
		x1 = x2;
	}	
	
	fclose(arq);
}

double fx(double x)
{
	double f;
	
	// dx/dt
	f = 0.001*x*(2-x);

	return f;
}

