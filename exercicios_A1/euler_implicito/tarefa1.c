#include<stdio.h>
#include<math.h>

void main()
{
	double x0=10;		// Initial conditions
	double t=0, x=x0; 
	double tf=20.;		// Final time 
	double h=10.;		// Time increment
	double lambda=0.2;	// Decay constant
	double xa=x0;

	printf("%lf %lf %lf \n",t,x,xa);

	// Loop for integration
	while(t<tf)
	{
		// x + f(x)
		x = x/(1+lambda*h);
		t = t+h;
		xa = x0*exp(-lambda*t);

		printf("%lf %lf %lf \n",t,x,xa);
		
	}	

}
