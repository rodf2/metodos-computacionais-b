#include<stdio.h>
#include<math.h>

// Integration of the deacayment problem using Runge-Kutta Second order method (Mean point)

void main()
{
	double x0=10;		// Initial conditions
	double t=0, x=x0; 
	double tf=3;		// Final time 
	double h=0.1;		// Time increment
	double lambda=2.0;	// Decay constant
	double a1=0, a2=1.;	// Constants, condition: a1+a2=1
	double k1, k2, p1, q11, phi;
	double xa=x0;

	p1 = 0.5/a2;
	q11 = p1;

	printf("%lf %lf %lf \n",t,x,xa);

	// Loop for integration
	while(t<tf)
	{
		// f(x_i)
		k1 = -lambda*x;
		
		// f(x_i + q_11*k1*h)	
		k2 = -lambda*(x+(q11*k1*h));
		
		phi=(a1*k1)+(a2*k2);		

		// x_(i+1) = x_i + phi(x_i,h)*h
		x = x+(phi*h);
		t = t+h;
		xa = x0*exp(-lambda*t);

		printf("%lf %lf %lf \n",t,x,xa);
		
	}	

}
