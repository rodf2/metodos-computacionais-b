reset

# Line styles
set style line 1 lt 1 pt 2 linecolor rgb "red"
set style line 2 lt 1 pt 10 linecolor rgb "blue"
set style line 3 lt 1 pt 5 linecolor rgb "yellow"
set style line 4 lt 1 pt 6 linecolor rgb "green"
set style line 5 lt 1 pt 10 linecolor rgb "orange"
set style line 6 lt 1 pt 2 linecolor rgb "cyan"
set style line 7 lt 1 pt 1 linewidth 2 linecolor rgb "black"

set encoding utf8
set title "Decaimento radioativo (lambda = 2)"
set xlabel "Tempo (t)"
set ylabel "Quantidade de material (x)"
set grid
#set key

set term post eps enhanced color
set out 'plots_7.eps'

plot "RK2_0.1.dat" u 1:2 w lp ls 1 title "RK2 (Pto. Médio), dt = 0.1", "RK2_0.5.dat" u 1:2 w lp ls 2 title "RK2 (Pto. Médio), dt = 0.5", "RK4_0.1.dat" u 1:2 w lp ls 5 title "RK4 (3/8), dt = 0.1", "RK4_0.5.dat" u 1:2 w lp ls 6 title "RK4 (3/8), dt = 0.5", "RK2_0.1.dat" u 1:3 w lp ls 7 title "Valor analítico"

#pause -1
