#include<stdio.h>
#include<math.h>

// Integration of the deacayment problem using Runge-Kutta RK4 (3/8 Butcher's table)

void main()
{
	double x0=10;		// Initial conditions
	double t=0, x=x0; 
	double tf=3;		// Final time 
	double h=0.5;		// Time increment
	double lambda=2.0;	// Decay constant
	double a1=1./8., a2=3./8., a3=3./8., a4=1./8. ;	// Coeficients, condition: a1+a2+a3+a4=1 
	//double p1=1./3., p2=2./3., p3=1.; Not used in this decayment problem
	double q11=1./3., q21=-1./3., q22=1., q31=1., q32=-1., q33=1. ; 
	double k1, k2, k3, k4, phi;  
	double xa=x0;

	// CREATE ARRAY HERE AFTER THE CLASS (instead of defining a bunch of variables...)

	printf("%lf %lf %lf \n",t,x,xa);

	// Loop for integration
	while(t<tf)
	{
		// f(x_i)
		k1 = -lambda*x;
		
		// f(x_i + q_11*k1*h)	
		k2 = -lambda*(x+(q11*k1*h));

		//
		k3 = -lambda*(x+(q21*k1*h)+(q22*k2*h));

		//
		k4 = -lambda*(x+(q31*k1*h)+(q32*k2*h)+(q33*k3*h));
		
		// Composition of derivatives
		phi=(a1*k1)+(a2*k2)+(a3*k3)+(a4*k4);		

		// x_(i+1) = x_i + phi(x_i,h)*h
		x = x+(phi*h);
		t = t+h;
		xa = x0*exp(-lambda*t);

		printf("%lf %lf %lf \n",t,x,xa);
		
	}	

}
