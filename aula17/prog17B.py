import numpy as np

# Function for Henon map
def f(a,x,y):
	f = a-(x*x)+y
	return f

def g(b,x):
	g = b*x
	return g


x0 = 0.0
y0 = 0.0

a = 0.2
b = -0.999
	
n = 10001	# Number of iterations for henon map
dtf = 'henon_a_'+str(a)+'_b_'+str(b)+'.dat'	# Data filename

arq = open(dtf,'w')

# Loop of n iterations
for i in range(1,n):
	
	x1 = f(a,x0,y0)
	y1 = g(b,x0)

	x0=x1
	y0=y1	

	arq.write(str(i)+'\t'+str(x1)+'\t'+str(y1)+'\n')

