import numpy as np

# X Henon map function
def f(a,x,y):
	f = a-(x*x)+y
	return f

# Y Henon map function
def g(b,x):
	g = b*x
	return g

# Fixed points function
def p_fix(a,b):

	# First X fixed point
	x_fix1 = (1./2.)*((b-1)+np.sqrt((b*b)-(2*b)+(4*a)+1))

	# Second X fixed point
	x_fix2 = (1./2.)*((b-1)-np.sqrt((b*b)-(2*b)+(4*a)+1))

	x_l = [x_fix1,x_fix2]	# Output is a list
	return x_l

# Function to calc. eigenvalues of the correspondent Jacobian matrix
def eig_J(a,b,xf):

	# The eigenvalues lambda follow the equation:
	#	lambda = -x_* +/- sqrt((x_*)^2 + b)
	# Where x_* is a fixed point

	lbd1 = -xf + np.sqrt((xf*xf)+b)	# 1st eigenvalue
	lbd2 = -xf - np.sqrt((xf*xf)+b)	# 2nd eigenvalue

	lbd_l = [lbd1,lbd2]	# Output is a list
	return lbd_l

###################################################################

# Initial values
x0 = 0.0
y0 = 0.0

a = 0.0		# Init. value for param. a
b = 0.3		# Fixed value for param. b

amin = 0.0	# Minimum value for a 
amax = 1.42	# Maximum value for a

da = (amax-amin)/300.	# Divide the interval in 300 parts

n = 10001	# Number of iterations

dtf = 'henon_b_'+str(b)+'_.dat'	# Data filename

arq = open(dtf,'w')

# Loop over 1000 values for lambda
while(a<=amax):

	x0 = 0.0	# Initialize x0
	y0 = 0.0	# Initialize y0

	# Loop over 1000 iterations for x_n
	for i in range(1,n):	
	
		x1 = f(a,x0,y0)		# Calc. x_{n+1}
		y1 = g(b,x0)		# Calc. y_{n+1}

		# Just saves values for n>=5000 (x_5000, y_5000)
		if(i<5000):	
			x0=x1	
			y0=y1

		else:
			xf1 = p_fix(a,b)[0]	# Calc. x_n fixed points
			xf2 = p_fix(a,b)[1]

			# Calc. the two eigenvalues for each fixed point
			lbd11 = eig_J(a,b,xf1)[0]
			lbd12 = eig_J(a,b,xf1)[1]
			lbd21 = eig_J(a,b,xf2)[0]
			lbd22 = eig_J(a,b,xf2)[1]

			arq.write(str(a)+'\t'+str(x1)+'\t'+str(xf1)+'\t'+str(xf2)+'\t')
			arq.write(str(lbd11)+'\t'+str(lbd12)+'\t'+str(lbd21)+'\t'+str(lbd22)+'\n')

			x0=x1
			y0=y1	

	# Increment parameter a
	a = a + da

arq.close()
