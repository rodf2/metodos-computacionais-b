reset 

set encoding utf8 
set xlabel "a" 
set ylabel "x_n" 
set grid 
set key left 
set term post eps enhanced color 
set xrange[0:1.5]

set out "henon_caos.eps" 

plot "henon_b_0.3_.dat" u 1:2 ps 0.2 lc rgb "red" title "Bifurcation", \
	"henon_b_0.3_.dat" u 1:3 ps 0.4 lc rgb "blue" title "1st fixed point (x_1)", \
	"henon_b_0.3_.dat" u 1:4 ps 0.4 lc rgb "orange" title "2nd fixed point (x_2)", \
	"henon_b_0.3_.dat" u 1:5 ps 0.4 lc rgb "black" title "x_1: 1st eigenvalue", \
	"henon_b_0.3_.dat" u 1:6 ps 0.4 lc rgb "brown" title "x_1: 2nd eigenvalue", \
	"henon_b_0.3_.dat" u 1:7 ps 0.4 lc rgb "grey" title "x_2: 1st eigenvalue", \
	"henon_b_0.3_.dat" u 1:8 ps 0.4 lc rgb "magenta" title "x_2: 2nd eigenvalue"
