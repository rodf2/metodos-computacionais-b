reset 

set encoding utf8 
set xlabel "n" 
set ylabel "x_n" 
set grid 
unset key 
set term post eps enhanced color 

set title "a=0.2, b=0.991"
set out "henon_xn_a_0.2_b_0.991.eps" 
plot "henon_a_0.2_b_0.991.dat" u 1:2:1 ps 0.3 palette

set title "a=0.2, b=-0.999"
set out "henon_xn_a_0.2_b_-0.999.eps" 
plot "henon_a_0.2_b_-0.999.dat" u 1:2:1 ps 0.3 palette

set title "a=1.4, b=0.3"
set out "henon_xn_a_1.4_b_0.3.eps" 
plot "henon_a_1.4_b_0.3.dat" u 1:2:1 ps 0.3 palette

set title "a=0.04, b=1.0"
set out "henon_xn_a_0.04_b_1.0.eps" 
plot "henon_a_0.04_b_1.0.dat" u 1:2:1 ps 0.3 palette
 
set ylabel "y_n" 

set title "a=0.2, b=0.991"
set out "henon_yn_a_0.2_b_0.991.eps" 
plot "henon_a_0.2_b_0.991.dat" u 1:3:1 ps 0.3 palette

set title "a=0.2, b=-0.999"
set out "henon_yn_a_0.2_b_-0.999.eps" 
plot "henon_a_0.2_b_-0.999.dat" u 1:3:1 ps 0.3 palette

set title "a=1.4, b=0.3"
set out "henon_yn_a_1.4_b_0.3.eps" 
plot "henon_a_1.4_b_0.3.dat" u 1:3:1 ps 0.3 palette

set title "a=0.04, b=1.0"
set out "henon_yn_a_0.04_b_1.0.eps" 
plot "henon_a_0.04_b_1.0.dat" u 1:3:1 ps 0.3 palette

