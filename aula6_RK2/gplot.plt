reset

# Line styles
set style line 1 lt 1 pt 2 linecolor rgb "red"
set style line 2 lt 1 pt 10 linecolor rgb "blue"
set style line 3 lt 1 pt 5 linecolor rgb "yellow"
set style line 4 lt 1 pt 6 linecolor rgb "green"
set style line 5 lt 1 pt 10 linecolor rgb "orange"
set style line 6 lt 1 pt 2 linecolor rgb "cyan"
set style line 7 lt 1 pt 1 linewidth 2 linecolor rgb "black"

set encoding utf8
set title "Decaimento radioativo (lambda = 2)"
set xlabel "Tempo (t)"
set ylabel "Quantidade de material (x)"
set grid
#set key

set term post eps enhanced color
set out 'plots_6.eps'

plot "Heun_0.1.dat" u 1:2 w lp ls 1 title "Mét. Heun, dt = 0.1", "Heun_0.5.dat" u 1:2 w lp ls 2 title "Mét. Heun, dt = 0.5", "Ralston_0.1.dat" u 1:2 w lp ls 3 title "Mét. Ralston, dt = 0.1", "Ralston_0.5.dat" u 1:2 w lp ls 4 title "Mét. Ralston, dt = 0.5", "pmed_0.1.dat" u 1:2 w lp ls 5 title "Mét. Pt. Médio, dt = 0.1", "pmed_0.5.dat" u 1:2 w lp ls 6 title "Mét. Pt. Médio, dt = 0.5", "pmed_0.9.dat" u 1:2 w lp ls 1 title "Mét. Pt. Médio, dt = 0.9", "pmed_1.0.dat" u 1:2 w lp ls 2 title "Mét. Pt. Médio, dt = 1.0", "Heun_0.1.dat" u 1:3 w lp ls 7 title "valor analítico"

#pause -1
