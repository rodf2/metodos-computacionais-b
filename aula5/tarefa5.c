#include<stdio.h>
#include<math.h>

// Pendulum (Verlet Method)

// x0 = x(t-dt)
// x1 = x(t)
// x2 = x(t+dt)

// Remember that x is the angle!

void main()
{
	double x1=3., v1=0.;		// Initial conditions
	double t=0., x=x1, v=v1, a, x0, x2; 
	double tf=30;			// Final time 
	double h=0.1;			// Time increment
	// Gravitational acceleration, lenght and mass
	double g=10., l=10., m=1.;
	double w;			// Angular frequency
	double K, U, E_t;		// Energy

	// Generalize to w = sqrt(g/l);
	w = 1;	

	// Notation: x = x(t) = x1

	// First values
	x0 = x-v*t;		// x at t-h
/*	K = m*l*l*v*v/2;	// Kinectic energy at t 
	U = m*g*l*(1-cos(x));	// Potential energy at t
	E_t = K+U;		// Total energy at t

	printf("%lf %lf %lf %lf %lf %lf %lf \n",t,x,v,a,K,U,E_t);
*/
	// Loop for integration
	while(t<tf)
	{
		a = -w*w*sin(x);	// a at t
		x2 = 2*x-x0+(a*h*h);	// x at t+h 
		v = (x2-x0)/(2*h);	// v at t
		K = m*l*l*v*v/2;	// Kinectic energy at t 
		U = m*g*l*(1-cos(x));	// Potential energy at t
		E_t = K+U;		// Total energy at t

		t=t+h;		

		printf("%lf %lf %lf %lf %lf %lf %lf \n",t,x,v,a,K,U,E_t);
		
		x0 = x;		
		x = x2;
	}	

}
