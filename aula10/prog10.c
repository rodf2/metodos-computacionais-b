#include<stdio.h>
#include<math.h>

// Integration of the logistic function using variable time increment
// dx/dt = f(x) = 0.001*x*(2-x)

double fx(double x_in);
double RK2(double x_in, double h_in);
double RK4(double x_in, double h_in);

void main()
{
	double x0=.001;		// Initial conditions
	double x_rk2, x_rk4;
	double t=0., x=x0; 
	double tf=4000.;		// Final time 
	double tol=pow(10,-6);	// Tolerance
	double h=0.5, h_new;	// Time increment
	double n=3.; 		// Order of error
	double m;		// Number of iterations
	double C=1500.;	// Constant
	double Ec=0;	// Error

	printf("%lf %lf %lf \n",t,x);

	// Loop for integration
	while(t<tf)
	{
		for(m=0; m<10; m=m+1)
		{
			x = RK2(x,h);
			t = t+h;
				
			printf("%lf %lf \n",t,x);
		}

		x_rk2 = RK2(x,h);
		x_rk4 = RK4(x,h);
		t = t+h;

		printf("%lf %lf \n",t,x_rk4);

		Ec = fabs(x_rk2-x_rk4);
		h_new = pow((tol/Ec),(1./n))*h;

		if(h_new>(2*h))
		{
			h=2*h;
		}
		else if(h_new<(h/2.))
		{
			h=h/2.;
		}
		else
		{
			h = h_new;
		}
	}	

}

double fx(double x_in)
{
	double f;
	
	// dx/dt
	f = 0.001*x_in*(2-x_in);

	return f;
}

double RK2(double x_in, double h_in)	// RK2 (Mean point) function
{
	double a1=0, a2=1., p1=0.5, q11= 0.5;
	double k1, k2, phi;	
	double x_out;
	
	// f(x_i)
	k1 = fx(x_in);

	// f(x_i + q_11*k1*h)
	k2 = fx(x_in+(q11*k1*h_in));

	phi = (a1*k1)+(a2*k2);

	// x_(i+1) = x_i + phi(x_i,h)*h
	x_out = x_in+(phi*h_in);

	return x_out;

}

double RK4(double x_in, double h_in)	// RK4 (Classic) function
{
	double a1=1/6., a2=1/3., a3=1/3., a4=1/6.;
	double q11=1/2., q21=0, q22=1/2., q31=0,q32=0,q33=1.;
	double k1, k2, k3, k4, phi;
	double x_out;
		
	// f(x_i)
	k1 = fx(x_in);

	// f(x_i + q11*k1*h)
	k2 = fx(x_in+(q11*k1*h_in));

	//f(x_i+q21*k1
	k3 = fx(x_in+(((q21*k1)+(q22*k2))*h_in));

	//f(
	k4 = fx(x_in+(((q31*k1)+(q32*k2)+(q33*k3))*h_in));

	phi = (a1*k1)+(a2*k2)+(a3*k3)+(a4*k4);

	// x_(i+1) = x_i + phi(x_i,h)*h
	x_out = x_in+(phi*h_in);

	return x_out;
}

// To see execution time: time ./a.out
