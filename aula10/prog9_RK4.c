#include<stdio.h>
#include<math.h>

// The problem of the last class generate error saturation for too low dt
// because the initial conditions were small. So, in this class we will solve
// an analogous linear problem for the pendulum, it is the, Simple Harmonic 
// Oscillator, d^2x/dt^2 = -w^2*x  

void main()
{
	double x0=100., v0=0.;		// Initial conditions
	double t=0., x=x0, v=v0; 
	double tf=30.;			// Final time 
	double h=pow(10,-6), hmax=2.;		// Time increment
	double w=1;
	double xa=x0, va=v0; 		// Analytical 
	double Egx, Egv, Egxm=0., Egvm=0.; 
	double k1x,k1v,k2x,k2v,k3x,k3v,k4x,k4v;

	// Loop to estimate global error for different h 
	while(h<hmax)
	{		
		t=0., x=x0, v=v0, Egx=0., Egv=0.;	// Initialize variables	

		// Loop for integration through RK2 (mean point)
		while(t<tf)
		{	
			
			k1x = v;		// f(x)
			k1v = -w*w*x;	// g(v)
			
			k2x = v+(k1v*h/2.);		// f(x+k1x*h/2)
			k2v = -w*w*(x+(k1x*h/2.));	// g(v+k1v*h/2)
			
			k3x = v+(k2v*h/2.);
			k3v = -w*w*(x+(k2x*h/2.));
			
			k4x = v+(k3v*h);
			k4v = -w*w*(x+(k3x*h));

			x = x+(((k1x+2*k2x+2*k3x+k4x)/6.)*h);	// x at the end of the interval
			v = v+(((k1v+2*k2v+2*k3v+k4v)/6.)*h);	// v at the end of the interval

			t = t+h;
			xa = x0*cos(w*t);
			va = -w*x0*sin(w*t);
		
			Egx = Egx+(x-xa);	// Position global error
			Egv = Egv+(v-va);	// Velocity global error
		}
		
		Egxm = Egx*(h/tf);	// Mean position global error
		Egvm = Egv*(h/tf);	// Mean velocity global error
	
		printf("%lf %lf %lf \n",log(h),log(fabs(Egxm)),log(fabs(Egvm)));
		
		h = 1.5*h;
	}	

}
