#include<stdio.h>
#include<math.h>


void main()
{
	double x0=0.001.;		// Initial conditions
	double t=0., x=x0; 
	double tf=40.;			// Final time 
	double h=pow(10,-6);		// Time increment
	double w=1;
	double xa=x0, va=v0; 		// Analytical 
	double Egx, Egv, Egxm=0., Egvm=0.; 
	double x_aux,v_aux;		// Position and velocity in the middle of the interval
	double k1x,k1v,k2x,k2v;
	
	t=0., x=x0, v=v0, Egx=0., Egv=0.;	// Initialize variables	

	// Loop for integration through RK2 (mean point)
	while(t<tf)
	{	
			
		k1x = v;		// f(x)
		k1v = -w*w*x;	// g(v)

		x_aux = x+(k1x*h/2.);	// x at middle of interval
		v_aux = v+(k1v*h/2.);	// v at middle of the interval
			
		k2x = v_aux;			// f(x+k1x*h/2)
		k2v = -w*w*x_aux;	// g(v+k1v*h/2)
			
		x = x+(k2x*h);	// x at the end of the interval
		v = v+(k2v*h);	// v at the end of the interval

		t = t+h;
		xa = x0*cos(w*t);
		va = -w*v0*sin(w*t);			

	}

}
