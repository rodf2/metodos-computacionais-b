reset 

set encoding utf8 
set xlabel "x" 
set ylabel "f(x)" 
set grid 
unset key 
set term post eps enhanced color 
set yrange[0:2000]
set style fill solid

set title "Histograma para FDP uniforme: f(x) = 1"
set out "uniforme.eps" 
set boxwidth 0.005	# delta1/2
plot 'uniforme.dat' with boxes

set title "Histograma para FDP senóide: f(x) = sen(x) "
set out "seno.eps" 
set boxwidth 0.0157	# delta2/2
plot 'seno.dat' with boxes

set title "Histograma para tentativa de transformação direta em sen(x)  "
set out "teste.eps" 
set boxwidth 0.0157	# delta3/2
plot 'teste.dat' with boxes

#pause -1
