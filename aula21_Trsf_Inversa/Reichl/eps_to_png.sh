for f in `ls *.eps`; do
	echo "Convertendo " $f
	convert -density 300 $f -flatten ${f%.*}.png;
done
