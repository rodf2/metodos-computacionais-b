reset 

set encoding utf8 
set title "Logistic map" 
set xlabel "x_n" 
set ylabel "x_{n+1}" 
set grid 
set key left 

set term post eps enhanced color 
set out "spider_grafico.eps" 

set xrange [-0.5:0.5] 
set yrange [0:0.5] 
f(x)=0.2+x*x 
plot f(x), "spider_grafico.dat" u 1:2 w lp title "u =0.2" 
 