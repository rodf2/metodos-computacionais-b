import numpy as np

mu = 0.2	# mu <= 1/4
x0 = 0.1	# 1 > x0 > 0

xf1 = 0.5*(1+(np.sqrt(1-(4*mu))))
xf2 = 0.5*(1-(np.sqrt(1-(4*mu))))

print("mu="+str(mu))
print("x0="+str(x0))
print("Pontos fixos:")
print("Inst. --> x="+str(xf1))
print("Est. --> x="+str(xf2))

arq = open("aranha.dat","w")

for i in range(0,40):

	x1 = mu+(x0*x0)

	arq.write(str(x0)+"\t"+str(x1)+"\n")
	arq.write(str(x1)+"\t"+str(x1)+"\n")
	
	x0=x1

arq.close()

print("Pronto (:")	
