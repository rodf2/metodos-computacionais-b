import os

def f(u,x):
	f = u+x**2
	return f

# Variaveis necessariamente definidas
u = 0.2
x0 = 0.1
lbl='spider_grafico'	# Label de arquivos

Ei = open(lbl+'.dat', 'w')

for i in range(0,30):
	x1 = f(u,x0)
	
	ei = str(x0)+'\t'+str(x1)+'\n'
	Ei.write(ei)

	ei = str(x1)+'\t'+str(x1)+'\n'
	Ei.write(ei)

	x0=x1

Ei.close()

# Escreve instrucoes p/ gnuplot
gp = open(lbl+'.gnu', 'w')

gp.write('reset \n')
gp.write('\n')
gp.write('set encoding utf8 \n')
gp.write('set title "Logistic map" \n')
gp.write('set xlabel "x_n" \n')
gp.write('set ylabel "x_{n+1}" \n')
gp.write('set grid \n')
gp.write('set key left \n')
gp.write('\n')
gp.write('set term post eps enhanced color \n')
gp.write('set out "'+lbl+'.eps" \n')
gp.write('\n')
gp.write('set xrange [-0.5:0.5] \n')
gp.write('set yrange [0:0.5] \n')
gp.write('f(x)='+str(u)+'+x*x \n')
gp.write('plot f(x), "'+lbl+'.dat" u 1:2 w lp title "u ='+str(u)+'" \n')
gp.write(' ')
gp.close()

# Executa gnuplot
os.system('gnuplot '+lbl+'.gnu')
