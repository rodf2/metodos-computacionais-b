reset 

set encoding utf8 
set xlabel "lambda" 
set ylabel "x_n" 
set grid 
set key left 
set term post eps enhanced color 

set out "lyapunov.eps" 
set yrange [-6:2]

plot "lyapunov_x0_0.5_.dat" u 1:2 ps 0.3 title "Coef. Lyapunov"

set out "caos_e_lyapunov.eps" 
set yrange [-1:1]

plot "dados_x0_0.5_.dat" u 2:3 ps 0.1 title "x0=0.5", "lyapunov_x0_0.5_.dat" u 1:2 ps 0.2 title "Coef. Lyapunov"

set out "caotico.eps" 

plot "dados_x0_0.5_.dat" u 2:3 ps 0.1 title "x0=0.5"

