
# Function for logistic map
def f(lbd,x):
	f = 4*lbd*x*(1-x)
	return f

x0 = 0.5
dtf = 'dados_x0_'+str(x0)+'_.dat'	# Data filename

arq = open(dtf,'w')

lbd=0.0	# Lambda

# Loop over 1000 values for lambda
while(lbd<=1):

	x0 = 0.5	# Initialize x0

	# Loop over 1000 iterations for x_n
	for i in range(1,1001):	
	
		x1 = f(lbd,x0)	
		
		# Just saves x_n values for n>=500 (x_500)
		if(i<500):	
			x0=x1	

		else:
			arq.write(str(i)+'\t'+str(lbd)+'\t'+str(x1)+'\n')
			x0=x1	

	# Increment lambda
	lbd=lbd+0.001

arq.close()
