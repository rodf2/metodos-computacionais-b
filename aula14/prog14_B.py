import numpy as np

# Function for logistic map
def f(lbd,x):
	f = 4*lbd*x*(1-x)
	return f

# First derivative of logistic map f'(x_n)
def f_lin(lbd,x):
	g = 4*lbd-(8*lbd*x)
	return g


x0 = 0.5	
n = 1001	# Number of iterations for logistic map
dtf = 'lyapunov_x0_'+str(x0)+'_.dat'	# Data filename

arq = open(dtf,'w')

lbd=0.0

# Loop over 1000 different values of lambda between 0 and 1
while(lbd<=1):

	x0 = 0.5	# Resets x0 and sum 
	S = 0.0

	# Loop of n iterations for the logistic map
	for i in range(1,n):
	
		x1 = f(lbd,x0)
		S = S + (np.log((np.fabs(f_lin(lbd,x1)))))	# Sum of ln|f'(x_n)| for all x_n
		x0=x1	
	
	lly = (1./n)*S	# Lyapunov coeficient

	arq.write(str(lbd)+'\t'+str(lly)+'\n')

	# Lambda increment
	lbd=lbd+0.001
