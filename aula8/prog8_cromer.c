#include<stdio.h>
#include<math.h>

void main()
{
	double x0=0.1, v0=0.;		// Initial conditions
	double t=0., x=x0, v=v0; 
	double tf=30.;			// Final time 
	double h=pow(10,-6), hmax=2.;		// Time increment
	double g=10, l=10;		// Gravitational acceleration
	double w=1;
	double xa=x0, va=v0; 		// Analytical 
	double Eg, Egm=0.; 

	// Loop to estimate global error for different h 
	while(h<hmax)
	{		
		t=0., x=x0, v=v0, Eg=0.;	// Initialize variables	

		// Loop for integration through Euler-Cromer
		while(t<tf)
		{
			// x + f(x)
			x = x+(v*h);
			v = v-(w*w*sin(x)*h);
			t = t+h;
			xa = x0*cos(w*t);
		
			Eg = Eg+(x-xa);	// Global error
		}
		
		Egm = Eg*(h/tf);	// Mean global error
	
		printf("%lf %lf %lf %lf \n",log(h),log(fabs(Egm)), h, fabs(Egm));
		
		h = 1.5*h;
	}	

}
