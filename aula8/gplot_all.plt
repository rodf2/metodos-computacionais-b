reset

# Line styles
set style line 1 lt 1 pt 2 linecolor rgb "red"
set style line 2 lt 1 pt 10 linecolor rgb "blue"
set style line 3 lt 1 pt 5 linecolor rgb "yellow"
set style line 4 lt 1 pt 6 linecolor rgb "green"
set style line 5 lt 1 pt 10 linecolor rgb "orange"
set style line 6 lt 1 pt 2 linecolor rgb "cyan"
set style line 7 lt 1 pt 1 linewidth 2 linecolor rgb "black"

set encoding utf8
set title "Comparação dos erros globais médios"
set xlabel "log(dt)"
set ylabel "log(Eg_med)"
set grid
set key left

set term post eps enhanced color
set out 'plot_all_8.eps'

plot "cromer.dat" u 1:2 w lp ls 1 title "Euler-Cromer", "RK2.dat" u 1:2 w lp ls 2 title "RK2", "RK4.dat" u 1:2 w lp ls 3 title "RK4"
#pause -1
