reset

# Line styles
set style line 1 lt 1 pt 2 linecolor rgb "red"
set style line 2 lt 1 pt 10 linecolor rgb "blue"
set style line 3 lt 1 pt 5 linecolor rgb "yellow"
set style line 4 lt 1 pt 6 linecolor rgb "green"
set style line 5 lt 1 pt 10 linecolor rgb "orange"
set style line 6 lt 1 pt 2 linecolor rgb "cyan"
set style line 7 lt 1 pt 1 linewidth 2 linecolor rgb "black"

set encoding utf8
set xlabel "log(dt)"
set ylabel "log(Eg_med)"
set grid
set key left
set term post eps enhanced color

set title "Ajuste"
set out 'fit_Cromer.eps'

set xrange [-4:0]
f(x) = a*x+b
fit f(x) 'cromer.dat' u 1:2 via a,b

plot "cromer.dat" u 1:2 w lp ls 2 title "Euler-Cromer", f(x) title sprintf("fit: a=%.5f, b=%.5f",a,b)

set out 'fit_RK2.eps'

set xrange [-2.5:0]
g(x) = c*x+d
fit g(x) 'RK2.dat' u 1:2 via c,d

plot "RK2.dat" u 1:2 w lp ls 2 title "RK2", g(x) title sprintf("fit: a=%.5f, b=%5.f",c,d)

set out 'fit_RK4.eps'

set xrange [-4:0]
h(x) = e*x+f
fit h(x) 'RK4.dat' u 1:2 via e,f

plot "RK4.dat" u 1:2 w lp ls 2 title "RK4", h(x) title sprintf("fit: a=%.5f, b=%.5f",e,f)

#pause -1
