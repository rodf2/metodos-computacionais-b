reset 

set encoding utf8
set title "Matriz de recorrência" 
set xlabel "i" 
set ylabel "j" 
unset key 

set term post eps enhanced color 

set out "matriz_lbd_0.2.eps" 
#splot "dados.dat"
plot "dados_lbd_0.2.dat" u 1:2:3 with points ps 0.4 pt 7 palette

set out "matriz_lbd_0.25.eps" 
plot "dados_lbd_0.25.dat" u 1:2:3 with points ps 0.4 pt 7 palette

set out "matriz_lbd_0.4.eps" 
plot "dados_lbd_0.4.dat" u 1:2:3 with points ps 0.4 pt 7 palette

set out "matriz_lbd_0.6.eps" 
plot "dados_lbd_0.6.dat" u 1:2:3 with points ps 0.4 pt 7 palette

set out "matriz_lbd_0.75.eps" 
plot "dados_lbd_0.75.dat" u 1:2:3 with points ps 0.4 pt 7 palette

set out "matriz_lbd_0.82.eps" 
plot "dados_lbd_0.82.dat" u 1:2:3 with points ps 0.4 pt 7 palette

set out "matriz_lbd_0.91.eps" 
plot "dados_lbd_0.91.dat" u 1:2:3 with points ps 0.4 pt 7 palette

set out "matriz_lbd_0.96.eps" 
plot "dados_lbd_0.96.dat" u 1:2:3 with points ps 0.4 pt 7 palette

set out "matriz_lbd_0.999.eps" 
plot "dados_lbd_0.999.dat" u 1:2:3 with points ps 0.4 pt 7 palette

