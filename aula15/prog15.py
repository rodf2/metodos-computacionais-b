import numpy as np

# Logistic map
def f(lbd,x):
	f = 4*lbd*x*(1-x)
	return f

# Recurrence matrix 
def Mf(v1,v2,r0):
	
	delta = np.fabs(v2-v1)

	if(delta<r0):
		e = 1
	else:
		e = 0
	return e

# Logistic map parameters
x0 = 0.1
lbd = 0.96

N = 1000	# Number of iterations
r0 = 0.0001	# Tolerance for "convergence"
X_l = []

dtf = 'dados_lbd_'+str(lbd)+'.dat'       # Data filename

arq = open(dtf,'w')

# Iteration for logistic map
for n in range(0,(N+1)):
	
	x1 = f(lbd,x0)
	X_l.append(x1)
	x0 = x1

# Iteration for recurrence matrix elements
for i in range(0,(N+1)):
	
	xi = X_l[i]

	for j in range(0,(N+1)):

		xj = X_l[j]
		arq.write(str(i)+'\t'+str(j)+'\t'+str(Mf(xi,xj,r0))+'\n')

arq.close()
