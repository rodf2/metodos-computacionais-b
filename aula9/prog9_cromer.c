#include<stdio.h>
#include<math.h>

// The problem of the last class generate error saturation for too low dt
// because the initial conditions were small. So, in this class we will solve
// an analogous linear problem for the pendulum, it is the, Simple Harmonic 
// Oscillator, d^2x/dt^2 = -w^2*x  

void main()
{
	double x0=100., v0=0.;		// Initial conditions
	double t=0., x=x0, v=v0; 
	double tf=30.;			// Final time 
	double h=pow(10,-6), hmax=2.;		// Time increment
	double w=1;
	double xa=x0, va=v0; 		// Analytical 
	double Egx, Egv, Egvm=0., Egxm=0.; 

	// Loop to estimate global error for different h 
	while(h<hmax)
	{		
		t=0., x=x0, v=v0, Egx=0., Egv=0.;	// Initialize variables	

		// Loop for integration through Euler-Cromer
		while(t<tf)
		{
			// x + f(x)
			x = x+(v*h);
			v = v-(w*w*x*h);
			t = t+h;
			xa = x0*cos(w*t);
			va = -w*sin(w*t);			

			Egx = Egx+(x-xa);	// Position global error
			Egv = Egv+(v-va);	// Velocity global error
		}
		
		Egxm = Egx*(h/tf);	// Mean position global error
		Egvm = Egv*(h/tf);	// Mean velocity global error
	
		printf("%lf %lf %lf \n",log(h),log(fabs(Egxm))),log(fabs(Egvm));
		
		h = 1.5*h;
	}	

}
