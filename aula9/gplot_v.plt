reset

# Line styles
set style line 1 lt 1 pt 2 linecolor rgb "red"
set style line 2 lt 1 pt 10 linecolor rgb "blue"
set style line 3 lt 1 pt 5 linecolor rgb "yellow"
set style line 4 lt 1 pt 6 linecolor rgb "green"
set style line 5 lt 1 pt 10 linecolor rgb "orange"
set style line 6 lt 1 pt 2 linecolor rgb "cyan"
set style line 7 lt 1 pt 1 linewidth 2 linecolor rgb "black"

set encoding utf8
set title "Comparação dos erros globais médios para velocidade"
set xlabel "log(dt)"
set ylabel "log(Eg_{med})"
set grid
set key left

set term post eps enhanced color
set out 'plot_all_v.eps'

set xrange [-14:-1]
f(x) = a*x+b
fit f(x) 'cromer.dat' u 1:3 via a,b

set xrange [-11:-1]
g(x) = c*x+d
fit g(x) 'RK2.dat' u 1:3 via c,d

set xrange [-6:0]
h(x) = e*x+f
fit h(x) 'RK4.dat' u 1:3 via e,f

set xrange [-14:2]
set yrange [-30:10]

plot "cromer.dat" u 1:3 w lp ls 1 title "Euler-Cromer", \
 	"RK2.dat" u 1:3 w lp ls 2 title "RK2", \
	"RK4.dat" u 1:3 w lp ls 3 title "RK4", \
	f(x) title sprintf("fit: Beta=%.5f, Alfa=%.5f",a,b), \
	g(x) title sprintf("fit: Beta=%.5f, Alfa=%.5f",c,d), \
	h(x) title sprintf("fit: Beta=%.5f, Alfa=%.5f",e,f)

#pause -1
