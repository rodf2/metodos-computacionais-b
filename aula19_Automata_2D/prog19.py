# Celular automata 2D
import random
import os

# Check each cell state in a given t
def X_ck(X,N,M):
	
	Xr = X	

	for n in range(1,N-1):
		for m in range(1,M-1):
			
			# Calculate number of neighbours
			NN = (X[n-1][m-1])+(X[n][m-1])+(X[n+1][m-1])+(X[n-1][m])+(X[n+1][m])+(X[n-1][m+1])+(X[n][m+1])+(X[n+1][m+1])
			if((X[n][m])==1):
				if(NN<2 or NN>3):	# if has less than 2 or more than 3 neighbours
					Xr[n][m]=0	# Die
					
				else:	# If has 2 or 3 neighbours 
					Xr[n][m]=1	# Lives in the next generation

			elif((X[n][m])==0):	# If cell is dead (=0)
				if(NN==3):	# if has 3 neighbours 
					Xr[n][m]=1	# Born
				else:
					pass	# Stay in the same state
	
	return Xr

# Fraction of alive cells
def X_sum(X,N,M):
	
	nac = 0.0 # Number of alive cells
	tot = float(N*M) # Number of total cells

	for n in range(0,N):
		for m in range(0,M):
			nac = nac + X[n][m]
	fac = float(nac/tot)

	return fac

#######################################################################################

N = 100	# number of cells in lines
M = 100	# number of cells in columns
T = 500	# number of time steps

Xi = []	# Initial Condition Matrix (ICM)
ip = 0.08 # Probability of a cell to be alive in the ICM (0<ip<1)

for i in range(0,N):
	Xi.append([])
	for j in range(0,M):	# If the cell is in the border
		if(i==0 or j==0 or i==(N-1) or j==(N-1)):
			Xi[i].append(0)		
		else:	# Generate random inputs for all other cells
			# Each number between 0 and 1 has equal probality of being rv
			rv = random.random()	
			if(rv<ip):
				Xi[i].append(1)
				print('alive, rv: '+str(rv))
			elif(rv>=ip):
				Xi[i].append(0)
				print('dead, rv: '+str(rv))


# Prefix filename
pfn = 'automato_i'+str(ip)+'_t'

# Remove previous files associated with same ip
os.system('rm '+pfn+'*.dat')	

arq = open(pfn+str(0)+'.dat','w')

# Fraction evolution file
pef = open('pop_evo_i'+str(ip)+'.dat','w')

for i in range(0,N):
	for j in range(0,M):
		arq.write(str(i)+'\t'+str(j)+'\t'+str(Xi[i][j])+'\n')
pef.write(str(0)+'\t'+str(X_sum(Xi,N,M))+'\n')

arq.close()

X0 = Xi

for t in range(1,T):

	# Check each cell and its neighbours, changing states accordingly with the rule
	X1 = X_ck(X0,N,M)

	arq = open(pfn+str(t)+'.dat','w')
	
	for i in range(0,N):
		for j in range(0,M):
			arq.write(str(i)+'\t'+str(j)+'\t'+str(X1[i][j])+'\n')
	pef.write(str(t)+'\t'+str(X_sum(X1,N,M))+'\n')	# Number of alive cells


	arq.close()

	X0 = X1

pef.close()
