reset 

set encoding utf8 
set xlabel "x_i" 
set ylabel "y_i" 
set grid 
unset key 
#set term post eps enhanced color 
set title "Autômato Celular Bidimensional: Jogo da vida"
#set out "automato_300.eps" 
set xrange[-1:101]
set yrange[-1:101]

do for [i=0:149] { 
plot 'automato_i0.1_t'.i.'.dat' u 1:2:3 w image; 
pause 0.5 }

pause -1
