reset 

set encoding utf8 
set xlabel "x_i" 
set ylabel "y_i" 
set grid 
unset key 
#set term post eps enhanced color 
set title "Autômato Celular Bidimensional: Jogo da vida"

#set out "automato_300.eps" 

plot "automato_t0.dat" u 1:2:3 w image 
plot "automato_t1.dat" u 1:2:3 w image 
plot "automato_t2.dat" u 1:2:3 w image 
plot "automato_t3.dat" u 1:2:3 w image 
plot "automato_t4.dat" u 1:2:3 w image 
plot "automato_t5.dat" u 1:2:3 w image 
plot "automato_t6.dat" u 1:2:3 w image 
plot "automato_t7.dat" u 1:2:3 w image 
plot "automato_t8.dat" u 1:2:3 w image 
plot "automato_t9.dat" u 1:2:3 w image 
plot "automato_t10.dat" u 1:2:3 w image 
plot "automato_t11.dat" u 1:2:3 w image 
plot "automato_t12.dat" u 1:2:3 w image 
plot "automato_t13.dat" u 1:2:3 w image 
plot "automato_t14.dat" u 1:2:3 w image 
plot "automato_t15.dat" u 1:2:3 w image 
plot "automato_t16.dat" u 1:2:3 w image 
plot "automato_t17.dat" u 1:2:3 w image 
plot "automato_t18.dat" u 1:2:3 w image 
plot "automato_t19.dat" u 1:2:3 w image 
plot "automato_t20.dat" u 1:2:3 w image 
plot "automato_t21.dat" u 1:2:3 w image 
plot "automato_t22.dat" u 1:2:3 w image 
plot "automato_t23.dat" u 1:2:3 w image 
plot "automato_t24.dat" u 1:2:3 w image 
plot "automato_t25.dat" u 1:2:3 w image 
plot "automato_t26.dat" u 1:2:3 w image 
plot "automato_t27.dat" u 1:2:3 w image 
plot "automato_t28.dat" u 1:2:3 w image 
plot "automato_t29.dat" u 1:2:3 w image 
plot "automato_t30.dat" u 1:2:3 w image 
plot "automato_t31.dat" u 1:2:3 w image 
plot "automato_t32.dat" u 1:2:3 w image 
plot "automato_t33.dat" u 1:2:3 w image 
plot "automato_t34.dat" u 1:2:3 w image 
plot "automato_t35.dat" u 1:2:3 w image 
plot "automato_t36.dat" u 1:2:3 w image 
plot "automato_t37.dat" u 1:2:3 w image 
plot "automato_t38.dat" u 1:2:3 w image 
plot "automato_t39.dat" u 1:2:3 w image 
plot "automato_t40.dat" u 1:2:3 w image 
plot "automato_t41.dat" u 1:2:3 w image 
plot "automato_t42.dat" u 1:2:3 w image 
plot "automato_t43.dat" u 1:2:3 w image 
plot "automato_t44.dat" u 1:2:3 w image 
plot "automato_t45.dat" u 1:2:3 w image 
plot "automato_t46.dat" u 1:2:3 w image 
plot "automato_t47.dat" u 1:2:3 w image 
plot "automato_t48.dat" u 1:2:3 w image 
plot "automato_t49.dat" u 1:2:3 w image 
plot "automato_t50.dat" u 1:2:3 w image 
plot "automato_t51.dat" u 1:2:3 w image 
plot "automato_t52.dat" u 1:2:3 w image 
plot "automato_t53.dat" u 1:2:3 w image 
plot "automato_t54.dat" u 1:2:3 w image 
plot "automato_t55.dat" u 1:2:3 w image 
plot "automato_t56.dat" u 1:2:3 w image 
plot "automato_t57.dat" u 1:2:3 w image 
plot "automato_t58.dat" u 1:2:3 w image 
plot "automato_t59.dat" u 1:2:3 w image 
plot "automato_t50.dat" u 1:2:3 w image 
plot "automato_t51.dat" u 1:2:3 w image 
plot "automato_t52.dat" u 1:2:3 w image 
plot "automato_t53.dat" u 1:2:3 w image 
plot "automato_t54.dat" u 1:2:3 w image 
plot "automato_t55.dat" u 1:2:3 w image 
plot "automato_t56.dat" u 1:2:3 w image 
plot "automato_t57.dat" u 1:2:3 w image 
plot "automato_t58.dat" u 1:2:3 w image 
plot "automato_t59.dat" u 1:2:3 w image 


pause -1
