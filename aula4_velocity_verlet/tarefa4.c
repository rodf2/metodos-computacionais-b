#include<stdio.h>
#include<math.h>

// Pendulum

void main()
{
	double x0=9.28, v0=-0.2;		// Initial conditions
	double t=0., x=x0, v=v0, a; 
	double tf=30;			// Final time 
	double h=0.1;			// Time increment
	// Gravitational acceleration, lenght and mass
	double g=10, l=10, m=1;		// Gravitational acceleration
	double w;			// Angular frequency
	double K, U, E_t;		// Energy

	// Generalize to w = sqrt(g/l);
	w = 1;	

	a = -w*w*sin(x);	// Initial a 
	K = m*l*l*v*v/2;	// Kinectic energy 
	U = m*g*l*(1-cos(x));	// Potential energy
	E_t = K+U;		// Total energy


	printf("%lf %lf %lf %lf %lf %lf %lf \n",t,x,v,a,K,U,E_t);

	// Loop for integration
	while(t<tf)
	{
		a = -w*w*sin(x);	// a in the beginning of the interval
		v = v+(a*h/2);		// v in the middle of the interval
		x = x+(v*h);		// x in the end of the interval	
		a = -w*w*sin(x);	// a in the end of the interval 
		v = v+(a*h/2);		// v in the end of the interval 
		
		K = m*l*l*v*v/2;	// Kinectic energy 
		U = m*g*l*(1-cos(x));	// Potential energy
		E_t = K+U;		// Total energy

		t=t+h;

		printf("%lf %lf %lf %lf %lf %lf %lf \n",t,x,v,a,K,U,E_t);
		
	}	

}
