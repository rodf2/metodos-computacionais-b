reset

# Line styles
set style line 1 lt 1 pt 2 linecolor rgb "red"
set style line 2 lt 1 pt 10 linecolor rgb "blue"
set style line 3 lt 1 pt 5 linecolor rgb "orange"
set style line 4 lt 1 pt 6 linecolor rgb "green"
set style line 5 lt 1 pt 1 linecolor rgb "black"

set encoding utf8
set title "Pêndulo simples"
set xlabel "Tempo (t)"
set ylabel "Energia"
set grid
set key center

set term post eps enhanced color
set out 'plots_E_t.eps'

plot "t0_1_v0_0.dat" u 1:7 w lp ls 1 title "theta_0 = 1; v_0 = 0", "t0_1.5_v0_0.dat" u 1:7 w lp ls 2 title "theta_0 = 1.5; v_0 = 0", "t0_3_v0_0.dat" u 1:7 w lp ls 3 title "theta_0 = 3; v_0 = 0", "t0_-9.28_v0_0.2.dat" u 1:7 w lp ls 4 title "theta_0 = -9.28; v_0 = 0.2", "t0_9.28_v0_-0.2.dat" u 1:7 w lp ls 5 title "theta_0 = 9.28; v_0 = 0.2"

#pause -1
