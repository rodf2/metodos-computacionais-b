reset

# Line styles
set style line 1 lt 1 pt 2 linecolor rgb "red"
set style line 2 lt 1 pt 10 linecolor rgb "blue"
set style line 3 lt 1 pt 5 linecolor rgb "yellow"
set style line 4 lt 1 pt 6 linecolor rgb "green"
set style line 5 lt 1 pt 1 linecolor rgb "black"

set encoding utf8
set title "Espaço de fase: Pêndulo"
set xlabel "theta"
set ylabel "dtheta/dt"
set grid
set key left

set term post eps enhanced color
set out 'plots_fase.eps'

plot "t0_1_v0_0.dat" u 2:3 w lp ls 1 title "theta_0 = 1; v_0 = 0", "t0_1.5_v0_0.dat" u 2:3 w lp ls 2 title "theta_0 = 1.5; v_0 = 0", "t0_3_v0_0.dat" u 2:3 w lp ls 3 title "theta_0 = 3; v_0 = 0", "t0_-9.28_v0_0.2.dat" u 2:3 w lp ls 4 title "theta_0 = -9.28; v_0 = 0.2", [74:] "t0_9.28_v0_-0.2.dat" u 2:3 w lp ls 5 title "theta_0 = 9.28; v_0 = -0.2"

#pause -1
