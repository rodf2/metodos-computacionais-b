# Integration via Monte Carlo methods
import random
import numpy as np

# Function to integrate
def f(x):
	val = x*x
	return val
# Known primitive of the function
def p(x):
	pri = x*x*x/3
	return pri

N = 100000 # Number of random samplings

xmin = 0.	# Integration limits
xmax = 3.

ymin = 0.	# Min. & max values in the integration region
ymax = 9.

# List to store f(x) values
fl = []
# Initialize sum of points that lie between f(x) and x-axis
S = 0. 
# Initialize variable for the first moment
fsum = 0.

arq1 = open('MC_pointsA.dat','w')
for i in range(0,N):
	x = random.random(xmin,xmax)	
	y = (ymax-ymin)*random.random()
	
	if(y>=0):
		if(y>f(x)):
			pass
		else:
			S=S+1
			fsum = fsum+f(x)
			fl.append(f(x))
			arq1.write('{0}\t{1}\n'.format(x,y))
			
	else:
		if(y<f(x)):
			pass
		else:
			S=S+1
			fsum = fsum+f(x)
			fl.append(f(x))
			arq1.write('{0}\t{1}\n'.format(x,y))
arq1.close()

mom1 = fsum/N
Serr = 0.
for k in range(0,len(fl)):
         Serr = Serr+(fl-fmed)**2

Ik = p(xmax)-p(xmin)
I1 = (float(S)/N)*(xmax-xmin)*(ymax-ymin)
I2 = mom1*(xmax-xmin)
err1 = np.sqrt((Ik-I1)**2)
err2 = np.sqrt(Serr/(N-1))

print('Method 1: "hit and miss"')
print('Integration value: {0}'.format(I1))
print('Error: {0}'.format(err1))

print('Method 2: "hit and miss"')
print('Integration value: {0}'.format(I2))
print('Error: {0}'.format(err2))

########## Convergence test ##########

arq2 = open('NvsIA.dat','w')

Nmin = 50
Nmax = 100000
dN = 50

Ni = Nmin

while(Ni<Nmax):
	S = 0
	for i in range(0,Ni):
		x = random.uniform(xmin,xmax)	
		y = (ymax-ymin)*random.random()
	
		if(y>=0):
			if(y>f(x)):
				pass
			else:
				S=S+1
				
		else:
			if(y<f(x)):
				pass
			else:
				S=S+1

	I = (float(S)/Ni)*(xmax-xmin)*(ymax-ymin)
	Ni = Ni + dN

	arq2.write(str(Ni)+'\t'+str(I)+'\n')
	
arq2.close()
