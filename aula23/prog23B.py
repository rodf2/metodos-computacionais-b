# Integration via Monte Carlo "hit and miss" method
import random
import numpy as np

# Function to integrate
def f(x):
	val = x*x
	return val
# Known primitive of the function
def p(x):
	pri = x*x*x/3
	return pri

N = 100000 # Number of random samplings

xmin = 0.	# Integration limits
xmax = 3.

ymin = 0.	# Min. & max values in the integration region
ymax = 9.

# Initialize sum of points that lie between f(x) and x-axis
S = 0 

arq1 = open('MC_pointsA.dat','w')
for i in range(0,N):
	x = (xmax-xmin)*random.random()	
	y = (ymax-ymin)*random.random()
	
	if(y>=0):
		if(y>f(x)):
			pass
		else:
			S=S+1
			arq1.write('{0}\t{1}\n'.format(x,y))
				
	else:
		if(y<f(x)):
			pass
		else:
			S=S+1
			arq1.write('{0}\t{1}\n'.format(x,y))
arq1.close()

Ival = (float(S)/N)*(xmax-xmin)*(ymax-ymin)
Ik = p(xmax)-p(xmin)
err = np.sqrt((Ik-Ival)**2)

print('Integration value: {0}'.format(Ival))
print('Error: {0}'.format(err))

########## Convergence test ##########

arq2 = open('NvsIA.dat','w')

Nmin = 50
Nmax = 100000
dN = 50

Ni = Nmin

while(Ni<Nmax):
	S = 0
	for i in range(0,Ni):
		x = (xmax-xmin)*random.random()	
		y = (ymax-ymin)*random.random()
	
		if(y>=0):
			if(y>f(x)):
				pass
			else:
				S=S+1
				
		else:
			if(y<f(x)):
				pass
			else:
				S=S+1

	I = (float(S)/Ni)*(xmax-xmin)*(ymax-ymin)
	Ni = Ni + dN

	arq2.write(str(Ni)+'\t'+str(I)+'\n')
	
arq2.close()
