reset

# Line styles
set style line 1 lt 1 pt 2 linecolor rgb "red"
set style line 2 lt 1 pt 10 linecolor rgb "blue"
set style line 3 lt 1 pt 5 linecolor rgb "orange"
set style line 4 lt 1 pt 6 linecolor rgb "green"
set style line 5 lt 1 linecolor rgb "black"

set encoding utf8
set title "Decaimento radioativo"
set xlabel "Tempo (t)"
set ylabel "Quantidade de material (x)"
set grid
set key right

set term post eps enhanced color
set out 'plots.eps'

g(x) = 10*exp(-2*x)

plot "out_001.dat" u 1:2 w lp ls 1 title "h = 0.01", "out_01.dat" u 1:2 w lp ls 2 title "h = 0.1", "out_05.dat" u 1:2 w lp ls 3 title "h = 0.5", "out_1.dat" u 1:2 w lp ls 4 title "h = 1", g(x) title "Analítico" linewidth 3 linecolor rgb "black"

#pause -1
