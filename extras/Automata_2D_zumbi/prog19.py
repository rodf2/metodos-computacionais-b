# Celular automata 2D with disease infection
import random
import os

# 1 is for alive cell, 0 for dead cell, 0.5 for infected cell

# Apply rules in a given matrix containing the cells
def X_ck(X,N,M):
	
	ti = 0	# Threshold for infection by contact
	pi = 0.97	# Probability of infection by contact (Infectability)
	psi = 0.00001	# Probability of spontaneuos infection (Disease arising)
	tc = 3 # Threshold for infection cure by contact
	pc = 0.7	# Probability of cure by contact (Curability)
	psd = 0.8	# Probability of death once a cell is infected (Lethality)
	psc = 0.3	# Probability of spontaneus cure (Disease weakness)

	Xr = X	

	for n in range(1,N-1):
		for m in range(1,M-1):
			
			# Calculate number of alive neighbours
			NN = 0
			for j in range(-1,2):	# Sum the value of 9x9 matrix centered in a cell
				for k in range(-1,2):
					NN = NN + int(X[n+j][m+k])
			NN = NN - int(X[n][m])	# Remove value of the own cell			

			# Calculate number of infected neighbours
			NZ = 0
			for j in range(-1,2):   # Sum the value of 9x9 matrix centered in a cell
                                for k in range(-1,2):
                                        if(X[n+j][m+k]==0.5):
						NZ = NZ+1						
					else:
						pass
			if(X[n][m]==0.5):
				NZ = NZ-1
			else:
				pass

			# Apply rules
			if((X[n][m])==1):	# If cell is alive
				rand1 = random.random()
				rand2 = random.random()
				if(NZ>tc and rand2<pi):	# Infection by contact
					Xr[n][m]=0.5
				elif(rand1<psi):	# Spontaneuos infection from environment
					Xr[n][m]=0.5
				else:
						if((NZ+NN)<2 or (NZ+NN)>3):	# if has less than 2 or more than 3 neighbours
							Xr[n][m]=0	# Die
						
						else:	# If has 2 or 3 alive 
							Xr[n][m]=1	# Lives in the next generation
			elif((X[n][m])==0.5):	# If cell is infected
				rand1 = random.random()
				rand2 = random.random()
				rand3 = random.random()
				if(NN>tc and rand1>=psd and rand3<pc):	# If has more than x alive neighbours and do not died
					Xr[n][m]=1	# Is cured and turn alive again
				elif(NN==0 and rand3>=pc):	# If it has no alive cell around and do not cured
					Xr[n][m]=0	# Die by isolation
				else:	# Spontaneous death or cure
					if(rand1<psd and rand2>psc):	# Death by the disease
						Xr[n][m]=0
					elif(rand1>psd and rand2<psc):	# Cure by defense system
						Xr[n][m]=1
					else:
						pass		
	
			elif((X[n][m])==0):	# If cell is dead
				if(NN==3):	# if has 3 alive neighbours 
					Xr[n][m]=1	# Born
				elif((NN+NZ)==3 and psd>0.25):	# if the disease is weak enough and has 3 neighbours
					Xr[n][m]=1
				else:
					pass	# Stay in the same state
	
	return Xr

# Calculates fraction of alive and infected cells
def X_frac(X,N,M):
	
	nac = 0.0	# Number of alive cells
	nzc = 0.0	# Number of infected cells
	tot = float(N*M) # Number of total cells

	for n in range(0,N):
		for m in range(0,M):
			if(X[n][m]==0.5):
				nzc = nzc + 1
			else:
				nac = nac + X[n][m]
	fac = float(nac/tot)
	fzc = float(nzc/tot)

	return fac,fzc

#######################################################################################

N = 230	# number of cells in lines
M = 230	# number of cells in columns
T = 700	# number of time steps

Xi = []	# Initial Condition Matrix (ICM)
ip = 0.5 	# Probability of a cell to be alive in the ICM (0<ip<1)
zp = 0.1	# Probability of a cell to be infected in the ICM (0<ip<1)

for i in range(0,N):
	Xi.append([])
	for j in range(0,M):	# If the cell is in the border
		if(i==0 or j==0 or i==(N-1) or j==(N-1)):
			Xi[i].append(0)		
		else:	# Generate random inputs for all other cells
			# Each number between 0 and 1 has equal probality of being rv
			rv = random.random()	
			if(rv<ip):
				zrv = random.random()
				if(zrv<zp):
					Xi[i].append(0.5)
                                	print('zombie, rv: '+str(rv))
				elif(zrv>=zp):
					Xi[i].append(1)
					print('alive, rv: '+str(rv))
			elif(rv>=ip):
				Xi[i].append(0)
				print('dead, rv: '+str(rv))


# Prefix filename
pfn = 'automato_i'+str(ip)+'_t'

# Remove previous files associated with same ip
os.system('rm '+pfn+'*.dat')	

arq = open(pfn+str(0)+'.dat','w')

# Fraction evolution file
pef = open('pop_evo_i'+str(ip)+'.dat','w')

for i in range(0,N):
	for j in range(0,M):
		arq.write(str(i)+'\t'+str(j)+'\t'+str(Xi[i][j])+'\n')
a_frac, z_frac = X_frac(Xi,N,M)
pef.write(str(0)+'\t'+str(a_frac)+'\t'+str(z_frac)+'\n')

arq.close()

X0 = Xi

for t in range(1,T):
	
	print('Time step: {0} of {1}'.format(t,T))

	# Check each cell and its neighbours, changing states accordingly with the rule
	X1 = X_ck(X0,N,M)

	arq = open(pfn+str(t)+'.dat','w')
	
	for i in range(0,N):
		for j in range(0,M):
			arq.write(str(i)+'\t'+str(j)+'\t'+str(X1[i][j])+'\n')
	
	a_frac, z_frac = X_frac(X1,N,M)	

	pef.write(str(t)+'\t'+str(a_frac)+'\t'+str(z_frac)+'\n')	# Fractions

	arq.close()

	X0 = X1

pef.close()
