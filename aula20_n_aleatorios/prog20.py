# Congruent random number generator

# Function to generate pseudo-random integer values 
def randGenerator(xi):
	a = 16807.
	q = 127773.
	r = 2836.
	m = 2147483647  	
	
	rx =a*(xi%q)-(r*(int(xi/q)))
	if(rx<0):
		rx =rx+m
		return rx
	else:
		return rx

N = 100000	# Number of random values
M = 100		# Number of divisions for the histogram
delta = 1./M
m = 2147483647.	# Parameter
X0 = 3141	# Initial guess	
X = []	# List of pseudo-random generated values
hist = []	# Histogram list

# Generate histogram elements
for j in range(0,M):
	hist.append(0)	

X.append(X0/m)
for i in range(1,N):	
	X1 = randGenerator(X0)
	X.append(X1/m)
	index = int(((X1/m)/M)*M*M)
	hist[index] = hist[index]+1
	X0 = X1

arq = open('aleatorios.dat','w')	
for k in range(0,M):
	x = (delta/2.)+k*delta
	arq.write(str(x)+'\t'+str(hist[k])+'\n')

arq.close()

print(max(X))
print(len(X))
print(len(hist))
