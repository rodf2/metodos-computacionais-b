reset 

set encoding utf8 
set xlabel "x" 
set ylabel "#" 
set grid 
unset key 
#set term post eps enhanced color 
set title "Gerador aleatório congruente"
set out "aleatorio.eps" 
set yrange[0:1500]
set boxwidth 0.005
set style fill solid

plot 'aleatorios.dat' with boxes

#pause -1
