reset

# Line styles
set style line 1 lt 1 pt 2 linecolor rgb "red"
set style line 2 lt 1 pt 10 linecolor rgb "blue"
set style line 3 lt 1 pt 5 linecolor rgb "orange"
set style line 4 lt 1 pt 6 linecolor rgb "green"
set style line 5 lt 1 linewidth 3 linecolor rgb "orange"

set encoding utf8
set title "Pêndulo simples"
set xlabel "Tempo (t)"
set ylabel "Ângulo"
set grid
set key left

set term post eps enhanced color
set out 'plots.eps'

plot "out_expl.dat" u 1:2 w lp ls 1 title "Euler Explícito", "out_cro.dat" u 1:2 w lp ls 2 title "Euler-Cromer", "out_cro.dat" u 1:3 w l ls 5 title "Analítico"

#pause -1
