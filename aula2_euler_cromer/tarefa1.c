#include<stdio.h>
#include<math.h>

// Integration with Euler-Cromer

void main()
{
	double x0=0.1, v0=0.;		// Initial conditions
	double t=0., x=x0, v=v0; 
	double tf=30;			// Final time 
	double h=0.1;			// Time increment
	double g=10, l=10;		// Gravitational acceleration
	double w=1;
	double xa=x0, va=v0; 		// Analytical 

	// w = sqrt(g/l);

	printf("%lf %lf %lf \n",t,x,xa);

	// Loop for integration
	while(t<tf)
	{
		// x + f(x)
		x = x+(v*h);
		v = v-(w*w*sin(x)*h);
		t = t+h;
		xa = x0*cos(w*t);

		printf("%lf %lf %lf \n",t,x,xa);
		
	}	

}
